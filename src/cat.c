#include <stdio.h>

int main(int argc, char *argv[]){
	char ch;
	for (int i = 1; i < argc; i++) {
		FILE *file = fopen(argv[i], "r");
		if (file == NULL ) {
			printf("Cannot find file: %s\n", argv[1]);
			return 1;
		}

		while ((ch = fgetc(file)) != EOF)
       		{
			putchar(ch);			
			}
		fclose(file);
	}
}
