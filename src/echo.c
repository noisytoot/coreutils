#include <stdio.h>
#include <string.h>
#include <stdbool.h>

int main(int argc, char *argv[]) {
  	bool newline = true; // Enables newline in the end (to disable use arg -n)
  	bool escapes = false; // Enables backslash-escaped characters (to enable use arg -e)
  	for (int i = 1; i < argc; i++) {
    	if (!strcmp(argv[i], "-n")) {
      		newline = false;
    	} else if (!strcmp(argv[i], "-e")) {
      		escapes = true;
    	} else if (!strcmp(argv[i], "-E")) {
      		escapes = false;
    	} else {
      		if (escapes) {
				for (size_t j = 0; j < strlen(argv[i]); j++) {
	  				if (argv[i][j] == '\\') {
	    				j++;
	    			switch(argv[i][j]) {
	    			case 'b':
	      				putchar('\b');
	      				break;
   	    			case 'c':
	      				return 0;
	    			case 'e':
	      				putchar('\e');
	      				break;
	    			case 'f':
	      				putchar('\f');
				      break;
    	    		case 'n':
	      				putchar('\n');
						break;
    	    		case 'r':
	      				putchar('\r');
	      				break;
    	    		case 't':
	      				putchar('\t');
	      				break;
	    			case 'v':
	      				putchar('\v');
	      				break;
	    			case '\\':
	      				putchar("\\");
	      				break;
	    			default:
	      				printf("\\%c", argv[i][j]);
	    			}
	  			} else {
	    			printf("%c", argv[i][j]);
	  			}
			}
 		} else {
			printf("%s", argv[i]);
		}
		if (i < argc - 1)
			putchar(' ');
    	}
  	}
  	if (newline) {
    	putchar('\n');
  	}
  	return 0;
}
